
use master
go
create database EuroMilhoes
go
use EuroMilhoes

create table EuroUser(
	id integer identity(1,1),
	name varchar(30) not null,
	email varchar(30) not null unique,
	primary key(id)
)


create table Ative_Key(
	eurokey varchar(25) not null,
	data datetime not null,
	EuroUserId integer references EuroUser(id),
	primary key(eurokey,data,EuroUserId)
)

create table Arqui_Key(
	eurokey varchar(25) not null,
	data datetime not null,
	EuroUserId integer references EuroUser(id) not null,
	primary key(eurokey,data,EuroUserId)
)
