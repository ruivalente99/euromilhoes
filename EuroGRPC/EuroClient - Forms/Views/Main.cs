﻿using EuroClient___Forms.Views;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EuroClient___Forms
{
    public partial class Main : Form
    {
        //Janela aberta no momento
        Form activeForm { get; set; }
        public Main()
        {
            InitializeComponent();
            //Abrir a janela de escolher a "role"  e inserir IP
            OpenChildForm(new ChooseRole());
        }
        #region Abrir as outras janelas
        // Abrir os forms no mesmo form //
        /*
           * Recebe como paramentros o Form que deseja colocar na Main
           * Não devolve nada
         */
        public void OpenChildForm(Form childForm)
        {

            if (activeForm != null)
                activeForm.Close();
            activeForm = childForm;
            childForm.TopLevel = false;
            childForm.FormBorderStyle = FormBorderStyle.None;
            childForm.Dock = DockStyle.Fill;
            this.childFormPanel.Controls.Add(childForm);
            this.childFormPanel.Tag = childForm;
            childForm.BringToFront();
            childForm.Show();
            
        }
        #endregion
    }
}
