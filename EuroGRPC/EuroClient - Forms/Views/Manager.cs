﻿using EuroClient___Forms.Proto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace EuroClient___Forms.Views
{
    public partial class Manager : Form
    {
        #region Listas
        //Lista de números
        public List<int> Numbers = new List<int>();
        //Lista de estrelas
        public List<int> Stars = new List<int>();
        #endregion
        public Manager()
        {
            InitializeComponent();
        }
        #region Tratamento das listas
        //Função para limpar as listas
        /* 
           * Não recebe nada, nao devolve nada
         */
        public void ClearLists()
        {
            Numbers.Clear();
            Stars.Clear();
        }
        //Função para ordenar as listas
        /* 
           * Não recebe nada, nao devolve nada
         */
        private void SortLists()
        {
            Numbers.Sort();
            Stars.Sort();
        }
        #endregion
        #region Tratamento das chaves
        //Função para verificar se as chaves inseridas são válida
        /* 
           * Recebe como parâmetros todas as TextBox com a Chave do euromilhões
           * Retorna true se for válido, se houver algum problema false
           * Insere uma MessageBox com o erro em questão
         */
        private bool IsKey(IEnumerable<TextBox> keys)
        {
            int count = 0;
            foreach (TextBox textBox in keys)
            {
                if (String.IsNullOrEmpty(textBox.Text))
                {
                    MessageBox.Show("ERRO, por favor certifique-se que está tudo preenchido");
                    return false;
                }
                if (!int.TryParse(textBox.Text, out _))
                {
                    MessageBox.Show("ERRO, por favor certifique-se que só insere numeros");
                    return false;
                }
                count++;
                if (count > 4 && count <= 6)
                {
                    if (Convert.ToInt32(textBox.Text) > 12 || Convert.ToInt32(textBox.Text) < 1)
                    {
                        MessageBox.Show("ERRO, Estrelas so podem conter valores de 1 a 12");
                        return false;
                    }
                    if (Stars.Contains(Convert.ToInt32(textBox.Text)))
                    {
                        MessageBox.Show("ERRO, por favor não repita os valores");
                        return false;
                    }
                    Stars.Add(Convert.ToInt32(textBox.Text));
                }
                else
                {
                    if (Convert.ToInt32(textBox.Text) > 50 || Convert.ToInt32(textBox.Text) < 1)
                    {
                        MessageBox.Show("ERRO, Numeros so podem conter valores de 1 a 50");
                        return false;
                    }
                    if (Numbers.Contains(Convert.ToInt32(textBox.Text)))
                    {
                        MessageBox.Show("ERRO, por favor não repita os valores");
                        return false;
                    }
                    Numbers.Add(Convert.ToInt32(textBox.Text));
                }
            }
            return true;
        }
        #endregion
        #region Tratamento das chaves para string
        //Função para transformar as listas numa string
        /* 
           * Não recebe nada
           * Devolve uma string com a chave N,N,N,N,N;E,E
         */
        private string EuroToString()
        {
            var send = String.Join(",", Numbers.ConvertAll<string>(delegate (int i) { return i.ToString(); }).ToArray());
            send += ";";
            send += String.Join(",", Stars.ConvertAll<string>(delegate (int i) { return i.ToString(); }).ToArray());
            return send;
        }
        #endregion
        #region Clique "Fazer sorteio"
        //Função do clique para consultar vencedor
        /* 
          * Recebe parâmetros o botao clicado
          * Não retorna nada
        */
        private async void btDoSorteio_Click(object sender, EventArgs e)
        {
            #region Chaves
            //Limpar a grid
            dataGridWinners.Rows.Clear();
            ClearLists();
            if (!IsKey(groupBoxAposta.Controls.OfType<TextBox>())) return;
            SortLists();
            var send = EuroToString();
            #endregion
            #region Enivar para o servidor
            var key = new Key { Key_ = send };
            var reply = await Program.client.ConsultWinnersAsync(key);
            #endregion
            #region Tratamento da reposta
            foreach (var user in reply.UserId)
                {
                    dataGridWinners.Rows.Add(user.Name, user.Email);
                }
            #endregion
        }
        #endregion
        #region Clique "Sair"
        // Função para retornar à janela de escolher role e ip;
        private void BtSair_Click(object sender, EventArgs e) => Program.mainView.OpenChildForm(new ChooseRole());
        #endregion
    }
}
