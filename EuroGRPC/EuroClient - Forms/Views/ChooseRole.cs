﻿using EuroClient___Forms.Proto;
using Grpc.Net.Client;
using System;
using System.Net.Http;
using System.Windows.Forms;

namespace EuroClient___Forms.Views
{
    public partial class ChooseRole : Form
    {
        public ChooseRole()
        {
            InitializeComponent();
        }
        #region Clique "Ir"
        // Função para tratar o clique da janela
        /*
           * Recebe como paramentros o sender, botao carregado, neste caso nao faz diferença
           * Não devolve nada
        */
        private void btGo_Click(object sender, EventArgs e)
        {
            #region Tratamento de dados
            // Verifica se o campo do IP está preenchido
            if (tbIP.Text=="")
            {
                MessageBox.Show("Insira o IP");
                    return;
            }
            //Verifica se o IP inserido já inclui o protocolo Https, caso contrario coloca na string o protocolo
            if(!tbIP.Text.Contains("https://"))
                Program.ip = "https://";
            // Adiciona o texto na variavel que mais tarde vai ser utilizada
            Program.ip += tbIP.Text;
            #endregion
            #region Conexão com o servidor 
            var httpHandler = new HttpClientHandler();
            // Return `true` to allow certificates that are untrusted/invalid
            httpHandler.ServerCertificateCustomValidationCallback =
                HttpClientHandler.DangerousAcceptAnyServerCertificateValidator;
            Program.channel = GrpcChannel.ForAddress(Program.ip,new GrpcChannelOptions { HttpHandler = httpHandler });
            Program.client = new EuroBet.EuroBetClient(Program.channel);
            #endregion
            #region Novas Janelas
            //Decide que janela será aberta, consoante o radioButton selecionado, como está numa groupBox, apenas um será selecionado
            if (rbtAdmin.Checked)   Program.mainView.OpenChildForm(new Admin());
            if(rbtCliente.Checked)  Program.mainView.OpenChildForm(new Client());
            if(rbtGestor.Checked)   Program.mainView.OpenChildForm(new Manager());
            #endregion
        }
        #endregion
    }
}
