﻿using EuroClient___Forms.Proto;
using System;
using System.Windows.Forms;

namespace EuroClient___Forms.Views
{
    public partial class Admin : Form
    {
        public Admin()
        {
            InitializeComponent();
        }
        #region Clique "Mostrar apostas"
        //Função do clique para mostrar todas as apostas
        /* 
          * Recebe parâmetros o botao clicado
          * Não retorna nada
        */
        private async void btUsersKey_Click(object sender, EventArgs e)
        {
            //Limpar  grid
            dataGridView1.Rows.Clear();
            #region Mandar para o servidor
            var reply = await Program.client.ConsultAllAsync(new All());
            #endregion
            #region Tratamento da reposta
            for (int i = 0; i < reply.Key.Count; i++)
            {
                dataGridView1.Rows.Add(reply.UserId[i].Name, reply.Key[i].Key_);
            }
            #endregion
        }
        #endregion
        #region Clique "Arquivar apostas"
        //Função do clique para arquivar as repostas
        /* 
          * Recebe parâmetros o botao clicado
          * Não retorna nada
        */
        private async void BT_2_ClickAsync(object sender, EventArgs e)
        {
            #region Mandar para o servidor
            var reply = await Program.client.ArquiveAsync(new All());
            #endregion
            #region Tratamento da resposta
            MessageBox.Show(reply.Msg);
            #endregion
        }
        #endregion
        #region Clique "Sair"
        // Função para retornar à janela de escolher role e ip;
        private void BtSair_Click(object sender, EventArgs e) => Program.mainView.OpenChildForm(new ChooseRole());
        #endregion
    }
}
