﻿
using System;

namespace EuroClient___Forms.Views
{
    partial class Manager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btSair = new System.Windows.Forms.Button();
            this.btDoSorteio = new System.Windows.Forms.Button();
            this.groupBoxAposta = new System.Windows.Forms.GroupBox();
            this.textBoxNum2 = new System.Windows.Forms.TextBox();
            this.textBoxNum3 = new System.Windows.Forms.TextBox();
            this.textBoxNum4 = new System.Windows.Forms.TextBox();
            this.textBoxNum5 = new System.Windows.Forms.TextBox();
            this.textBoxEst1 = new System.Windows.Forms.TextBox();
            this.textBoxEst2 = new System.Windows.Forms.TextBox();
            this.textBoxNum1 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dataGridWinners = new System.Windows.Forms.DataGridView();
            this.Nome = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Email = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBoxAposta.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridWinners)).BeginInit();
            this.SuspendLayout();
            // 
            // btSair
            // 
            this.btSair.Font = new System.Drawing.Font("Ubuntu", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btSair.ForeColor = System.Drawing.Color.Black;
            this.btSair.Location = new System.Drawing.Point(55, 611);
            this.btSair.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btSair.Name = "btSair";
            this.btSair.Size = new System.Drawing.Size(179, 63);
            this.btSair.TabIndex = 15;
            this.btSair.Text = "Sair";
            this.btSair.UseVisualStyleBackColor = true;
            this.btSair.Click += new System.EventHandler(this.BtSair_Click);
            // 
            // btDoSorteio
            // 
            this.btDoSorteio.Font = new System.Drawing.Font("Ubuntu", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btDoSorteio.ForeColor = System.Drawing.Color.Black;
            this.btDoSorteio.Location = new System.Drawing.Point(215, 277);
            this.btDoSorteio.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btDoSorteio.Name = "btDoSorteio";
            this.btDoSorteio.Size = new System.Drawing.Size(179, 63);
            this.btDoSorteio.TabIndex = 13;
            this.btDoSorteio.Text = "Fazer Sorteio";
            this.btDoSorteio.UseVisualStyleBackColor = true;
            this.btDoSorteio.Click += new System.EventHandler(this.btDoSorteio_Click);
            // 
            // groupBoxAposta
            // 
            this.groupBoxAposta.Controls.Add(this.textBoxNum2);
            this.groupBoxAposta.Controls.Add(this.textBoxNum3);
            this.groupBoxAposta.Controls.Add(this.textBoxNum4);
            this.groupBoxAposta.Controls.Add(this.textBoxNum5);
            this.groupBoxAposta.Controls.Add(this.textBoxEst1);
            this.groupBoxAposta.Controls.Add(this.textBoxEst2);
            this.groupBoxAposta.Controls.Add(this.textBoxNum1);
            this.groupBoxAposta.Controls.Add(this.label2);
            this.groupBoxAposta.Controls.Add(this.label3);
            this.groupBoxAposta.Font = new System.Drawing.Font("Ubuntu", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.groupBoxAposta.ForeColor = System.Drawing.Color.Black;
            this.groupBoxAposta.Location = new System.Drawing.Point(72, 85);
            this.groupBoxAposta.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBoxAposta.Name = "groupBoxAposta";
            this.groupBoxAposta.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBoxAposta.Size = new System.Drawing.Size(512, 184);
            this.groupBoxAposta.TabIndex = 12;
            this.groupBoxAposta.TabStop = false;
            this.groupBoxAposta.Text = "Aposta";
            // 
            // textBoxNum2
            // 
            this.textBoxNum2.Location = new System.Drawing.Point(96, 117);
            this.textBoxNum2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.textBoxNum2.Name = "textBoxNum2";
            this.textBoxNum2.Size = new System.Drawing.Size(29, 30);
            this.textBoxNum2.TabIndex = 12;
            // 
            // textBoxNum3
            // 
            this.textBoxNum3.Location = new System.Drawing.Point(133, 117);
            this.textBoxNum3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.textBoxNum3.Name = "textBoxNum3";
            this.textBoxNum3.Size = new System.Drawing.Size(29, 30);
            this.textBoxNum3.TabIndex = 11;
            // 
            // textBoxNum4
            // 
            this.textBoxNum4.Location = new System.Drawing.Point(169, 117);
            this.textBoxNum4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.textBoxNum4.Name = "textBoxNum4";
            this.textBoxNum4.Size = new System.Drawing.Size(29, 30);
            this.textBoxNum4.TabIndex = 10;
            // 
            // textBoxNum5
            // 
            this.textBoxNum5.Location = new System.Drawing.Point(206, 117);
            this.textBoxNum5.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.textBoxNum5.Name = "textBoxNum5";
            this.textBoxNum5.Size = new System.Drawing.Size(29, 30);
            this.textBoxNum5.TabIndex = 9;
            // 
            // textBoxEst1
            // 
            this.textBoxEst1.Location = new System.Drawing.Point(313, 117);
            this.textBoxEst1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.textBoxEst1.Name = "textBoxEst1";
            this.textBoxEst1.Size = new System.Drawing.Size(29, 30);
            this.textBoxEst1.TabIndex = 8;
            // 
            // textBoxEst2
            // 
            this.textBoxEst2.Location = new System.Drawing.Point(350, 117);
            this.textBoxEst2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.textBoxEst2.Name = "textBoxEst2";
            this.textBoxEst2.Size = new System.Drawing.Size(29, 30);
            this.textBoxEst2.TabIndex = 7;
            // 
            // textBoxNum1
            // 
            this.textBoxNum1.Location = new System.Drawing.Point(59, 117);
            this.textBoxNum1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.textBoxNum1.Name = "textBoxNum1";
            this.textBoxNum1.Size = new System.Drawing.Size(29, 30);
            this.textBoxNum1.TabIndex = 6;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Ubuntu", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(93, 56);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(128, 33);
            this.label2.TabIndex = 2;
            this.label2.Text = "Números";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Ubuntu", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(298, 56);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(111, 33);
            this.label3.TabIndex = 3;
            this.label3.Text = "Estrelas";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Ubuntu", 32F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(608, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(642, 63);
            this.label1.TabIndex = 11;
            this.label1.Text = "Apostadores Vencedores";
            // 
            // dataGridWinners
            // 
            this.dataGridWinners.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridWinners.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridWinners.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Nome,
            this.Email});
            this.dataGridWinners.Location = new System.Drawing.Point(755, 115);
            this.dataGridWinners.Margin = new System.Windows.Forms.Padding(0);
            this.dataGridWinners.Name = "dataGridWinners";
            this.dataGridWinners.RowHeadersWidth = 51;
            this.dataGridWinners.RowTemplate.Height = 25;
            this.dataGridWinners.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dataGridWinners.Size = new System.Drawing.Size(303, 416);
            this.dataGridWinners.TabIndex = 16;
            // 
            // Nome
            // 
            this.Nome.HeaderText = "Nome";
            this.Nome.MinimumWidth = 6;
            this.Nome.Name = "Nome";
            this.Nome.Width = 125;
            // 
            // Email
            // 
            this.Email.HeaderText = "Email";
            this.Email.MinimumWidth = 6;
            this.Email.Name = "Email";
            this.Email.Width = 125;
            // 
            // Manager
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1280, 720);
            this.Controls.Add(this.dataGridWinners);
            this.Controls.Add(this.btSair);
            this.Controls.Add(this.btDoSorteio);
            this.Controls.Add(this.groupBoxAposta);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "Manager";
            this.Text = "Manager";
            this.groupBoxAposta.ResumeLayout(false);
            this.groupBoxAposta.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridWinners)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btSair;
        private System.Windows.Forms.Button btDoSorteio;
        private System.Windows.Forms.GroupBox groupBoxAposta;
        private System.Windows.Forms.TextBox textBoxNum2;
        private System.Windows.Forms.TextBox textBoxNum3;
        private System.Windows.Forms.TextBox textBoxNum4;
        private System.Windows.Forms.TextBox textBoxNum5;
        private System.Windows.Forms.TextBox textBoxEst1;
        private System.Windows.Forms.TextBox textBoxEst2;
        private System.Windows.Forms.TextBox textBoxNum1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dataGridWinners;
        private System.Windows.Forms.DataGridViewTextBoxColumn Nome;
        private System.Windows.Forms.DataGridViewTextBoxColumn Email;
    }
}