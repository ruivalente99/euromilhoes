﻿
namespace EuroClient___Forms.Views
{
    partial class Client
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listBoxApostas = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxNome = new System.Windows.Forms.TextBox();
            this.textBoxMail = new System.Windows.Forms.TextBox();
            this.groupBoxAposta = new System.Windows.Forms.GroupBox();
            this.gbKey = new System.Windows.Forms.GroupBox();
            this.textBoxNum2 = new System.Windows.Forms.TextBox();
            this.textBoxNum3 = new System.Windows.Forms.TextBox();
            this.textBoxNum4 = new System.Windows.Forms.TextBox();
            this.textBoxNum1 = new System.Windows.Forms.TextBox();
            this.textBoxNum5 = new System.Windows.Forms.TextBox();
            this.textBoxEst2 = new System.Windows.Forms.TextBox();
            this.textBoxEst1 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.btAddAposta = new System.Windows.Forms.Button();
            this.btLoadApostas = new System.Windows.Forms.Button();
            this.btSair = new System.Windows.Forms.Button();
            this.groupBoxAposta.SuspendLayout();
            this.gbKey.SuspendLayout();
            this.SuspendLayout();
            // 
            // listBoxApostas
            // 
            this.listBoxApostas.Font = new System.Drawing.Font("Ubuntu", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.listBoxApostas.FormattingEnabled = true;
            this.listBoxApostas.ItemHeight = 29;
            this.listBoxApostas.Location = new System.Drawing.Point(558, 71);
            this.listBoxApostas.Name = "listBoxApostas";
            this.listBoxApostas.Size = new System.Drawing.Size(457, 439);
            this.listBoxApostas.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Ubuntu", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label1.Location = new System.Drawing.Point(576, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(414, 56);
            this.label1.TabIndex = 1;
            this.label1.Text = "Chaves Apostadas";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Ubuntu", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label2.Location = new System.Drawing.Point(49, 29);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(98, 26);
            this.label2.TabIndex = 2;
            this.label2.Text = "Números";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Ubuntu", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label3.Location = new System.Drawing.Point(229, 29);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(88, 26);
            this.label3.TabIndex = 3;
            this.label3.Text = "Estrelas";
            // 
            // textBoxNome
            // 
            this.textBoxNome.Font = new System.Drawing.Font("Ubuntu", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.textBoxNome.Location = new System.Drawing.Point(24, 139);
            this.textBoxNome.Name = "textBoxNome";
            this.textBoxNome.PlaceholderText = "Nome";
            this.textBoxNome.Size = new System.Drawing.Size(346, 35);
            this.textBoxNome.TabIndex = 4;
            this.textBoxNome.Tag = "";
            // 
            // textBoxMail
            // 
            this.textBoxMail.Font = new System.Drawing.Font("Ubuntu", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.textBoxMail.Location = new System.Drawing.Point(24, 180);
            this.textBoxMail.Name = "textBoxMail";
            this.textBoxMail.PlaceholderText = "Mail";
            this.textBoxMail.Size = new System.Drawing.Size(346, 35);
            this.textBoxMail.TabIndex = 5;
            this.textBoxMail.Tag = "";
            // 
            // groupBoxAposta
            // 
            this.groupBoxAposta.Controls.Add(this.gbKey);
            this.groupBoxAposta.Controls.Add(this.textBoxMail);
            this.groupBoxAposta.Controls.Add(this.textBoxNome);
            this.groupBoxAposta.Font = new System.Drawing.Font("Ubuntu", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.groupBoxAposta.Location = new System.Drawing.Point(49, 57);
            this.groupBoxAposta.Name = "groupBoxAposta";
            this.groupBoxAposta.Size = new System.Drawing.Size(448, 239);
            this.groupBoxAposta.TabIndex = 6;
            this.groupBoxAposta.TabStop = false;
            this.groupBoxAposta.Text = "Aposta";
            // 
            // gbKey
            // 
            this.gbKey.Controls.Add(this.textBoxNum2);
            this.gbKey.Controls.Add(this.label2);
            this.gbKey.Controls.Add(this.textBoxNum3);
            this.gbKey.Controls.Add(this.label3);
            this.gbKey.Controls.Add(this.textBoxNum4);
            this.gbKey.Controls.Add(this.textBoxNum1);
            this.gbKey.Controls.Add(this.textBoxNum5);
            this.gbKey.Controls.Add(this.textBoxEst2);
            this.gbKey.Controls.Add(this.textBoxEst1);
            this.gbKey.Location = new System.Drawing.Point(24, 25);
            this.gbKey.Name = "gbKey";
            this.gbKey.Size = new System.Drawing.Size(346, 112);
            this.gbKey.TabIndex = 13;
            this.gbKey.TabStop = false;
            // 
            // textBoxNum2
            // 
            this.textBoxNum2.Location = new System.Drawing.Point(52, 75);
            this.textBoxNum2.Name = "textBoxNum2";
            this.textBoxNum2.Size = new System.Drawing.Size(26, 26);
            this.textBoxNum2.TabIndex = 12;
            // 
            // textBoxNum3
            // 
            this.textBoxNum3.Location = new System.Drawing.Point(84, 75);
            this.textBoxNum3.Name = "textBoxNum3";
            this.textBoxNum3.Size = new System.Drawing.Size(26, 26);
            this.textBoxNum3.TabIndex = 11;
            // 
            // textBoxNum4
            // 
            this.textBoxNum4.Location = new System.Drawing.Point(116, 75);
            this.textBoxNum4.Name = "textBoxNum4";
            this.textBoxNum4.Size = new System.Drawing.Size(26, 26);
            this.textBoxNum4.TabIndex = 10;
            // 
            // textBoxNum1
            // 
            this.textBoxNum1.Location = new System.Drawing.Point(20, 75);
            this.textBoxNum1.Name = "textBoxNum1";
            this.textBoxNum1.Size = new System.Drawing.Size(26, 26);
            this.textBoxNum1.TabIndex = 6;
            // 
            // textBoxNum5
            // 
            this.textBoxNum5.Location = new System.Drawing.Point(148, 75);
            this.textBoxNum5.Name = "textBoxNum5";
            this.textBoxNum5.Size = new System.Drawing.Size(26, 26);
            this.textBoxNum5.TabIndex = 9;
            // 
            // textBoxEst2
            // 
            this.textBoxEst2.Location = new System.Drawing.Point(274, 75);
            this.textBoxEst2.Name = "textBoxEst2";
            this.textBoxEst2.Size = new System.Drawing.Size(26, 26);
            this.textBoxEst2.TabIndex = 7;
            // 
            // textBoxEst1
            // 
            this.textBoxEst1.Location = new System.Drawing.Point(242, 75);
            this.textBoxEst1.Name = "textBoxEst1";
            this.textBoxEst1.Size = new System.Drawing.Size(26, 26);
            this.textBoxEst1.TabIndex = 8;
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(318, 122);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(26, 23);
            this.textBox3.TabIndex = 9;
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(326, 130);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(26, 23);
            this.textBox4.TabIndex = 10;
            // 
            // textBox5
            // 
            this.textBox5.Location = new System.Drawing.Point(334, 138);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(26, 23);
            this.textBox5.TabIndex = 11;
            // 
            // textBox6
            // 
            this.textBox6.Location = new System.Drawing.Point(48, 68);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(26, 23);
            this.textBox6.TabIndex = 12;
            // 
            // btAddAposta
            // 
            this.btAddAposta.Font = new System.Drawing.Font("Ubuntu", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btAddAposta.Location = new System.Drawing.Point(49, 309);
            this.btAddAposta.Name = "btAddAposta";
            this.btAddAposta.Size = new System.Drawing.Size(157, 47);
            this.btAddAposta.TabIndex = 7;
            this.btAddAposta.Text = "Criar Aposta";
            this.btAddAposta.UseVisualStyleBackColor = true;
            this.btAddAposta.Click += new System.EventHandler(this.btAddAposta_Click);
            // 
            // btLoadApostas
            // 
            this.btLoadApostas.Font = new System.Drawing.Font("Ubuntu", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btLoadApostas.Location = new System.Drawing.Point(220, 309);
            this.btLoadApostas.Name = "btLoadApostas";
            this.btLoadApostas.Size = new System.Drawing.Size(157, 47);
            this.btLoadApostas.TabIndex = 8;
            this.btLoadApostas.Text = "Carregar Apostas";
            this.btLoadApostas.UseVisualStyleBackColor = true;
            this.btLoadApostas.Click += new System.EventHandler(this.btLoadApostas_Click);
            // 
            // btSair
            // 
            this.btSair.Font = new System.Drawing.Font("Ubuntu", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btSair.Location = new System.Drawing.Point(49, 457);
            this.btSair.Name = "btSair";
            this.btSair.Size = new System.Drawing.Size(157, 47);
            this.btSair.TabIndex = 9;
            this.btSair.Text = "Sair";
            this.btSair.UseVisualStyleBackColor = true;
            this.btSair.Click += new System.EventHandler(this.BtSair_Click);
            // 
            // Client
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(1120, 540);
            this.Controls.Add(this.btSair);
            this.Controls.Add(this.btLoadApostas);
            this.Controls.Add(this.btAddAposta);
            this.Controls.Add(this.groupBoxAposta);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.listBoxApostas);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Client";
            this.Text = "Client";
            this.groupBoxAposta.ResumeLayout(false);
            this.groupBoxAposta.PerformLayout();
            this.gbKey.ResumeLayout(false);
            this.gbKey.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox listBoxApostas;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxNome;
        private System.Windows.Forms.TextBox textBoxMail;
        private System.Windows.Forms.GroupBox groupBoxAposta;
        private System.Windows.Forms.TextBox textBoxNum1;
        private System.Windows.Forms.TextBox textBoxNum2;
        private System.Windows.Forms.TextBox textBoxNum3;
        private System.Windows.Forms.TextBox textBoxNum4;
        private System.Windows.Forms.TextBox textBoxNum5;
        private System.Windows.Forms.TextBox textBoxEst1;
        private System.Windows.Forms.TextBox textBoxEst2;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.Button btAddAposta;
        private System.Windows.Forms.Button btLoadApostas;
        private System.Windows.Forms.Button btSair;
        private System.Windows.Forms.GroupBox gbKey;
    }
}
