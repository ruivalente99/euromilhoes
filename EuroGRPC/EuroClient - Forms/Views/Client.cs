﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using EuroClient___Forms.Proto;

namespace EuroClient___Forms.Views
{
    public partial class Client : Form
    {
        #region Listas
        //Lista de números
        public List<int> Numbers = new List<int>();
        //Lista de estrelas
        public List<int> Stars = new List<int>();
        #endregion
        public Client()
        {
            InitializeComponent();
        }
        #region Tratamento das listas
            //Função para limpar as listas
            /* 
               * Não recebe nada, nao devolve nada
             */
            public void ClearLists()
            {
                Numbers.Clear();
                Stars.Clear();
            }
            //Função para ordenar as listas
            /* 
               * Não recebe nada, nao devolve nada
             */
            private void SortLists()
            {
                Numbers.Sort();
                Stars.Sort();
            }
        #endregion
        #region Tratamento dos dados e das chaves
        //Função para verificar se as chaves inseridas são válida
        /* 
           * Recebe como parâmetros todas as TextBox com a Chave do euromilhões
           * Retorna true se for válido, se houver algum problema false
           * Insere uma MessageBox com o erro em questão
         */
        private bool IsKey(IEnumerable<TextBox> keys)
        {
            int count = 0;
            foreach (TextBox textBox in keys)
            {
                if (String.IsNullOrEmpty(textBox.Text))
                {
                    MessageBox.Show("ERRO, por favor certifique-se que está tudo preenchido");
                    return false;
                }
                if (!int.TryParse(textBox.Text, out _))
                {
                    MessageBox.Show("ERRO, por favor certifique-se que só insere numeros");
                    return false;
                }
                count++;
                if (count > 5)
                {
                    if (Convert.ToInt32(textBox.Text) > 12 || Convert.ToInt32(textBox.Text) < 1)
                    {
                        MessageBox.Show("ERRO, Estrelas so podem conter valores de 1 a 12");
                        return false;
                    }
                    if (Stars.Contains(Convert.ToInt32(textBox.Text)))
                    {
                        MessageBox.Show("ERRO, por favor não repita os valores");
                        return false;
                    }
                    Stars.Add(Convert.ToInt32(textBox.Text));
                }
                else
                {
                    if (Convert.ToInt32(textBox.Text) > 50 || Convert.ToInt32(textBox.Text) < 1)
                    {
                        MessageBox.Show("ERRO, Numeros so podem conter valores de 1 a 50");
                        return false;
                    }
                    if (Numbers.Contains(Convert.ToInt32(textBox.Text)))
                    {
                        MessageBox.Show("ERRO, por favor não repita os valores");
                        return false;
                    }
                    Numbers.Add(Convert.ToInt32(textBox.Text));
                }
            }
            return true;
        }
        //Função para verificar se os dados(nome, email), são válidos
        /* 
           * Retorna true se for válido, se houver algum problema false
           * Insere uma MessageBox com o erro em questão
         */
        public bool IsDataValid()
        {
            if (String.IsNullOrEmpty(textBoxNome.Text))
            {
                MessageBox.Show("Insira o seu nome");
                return false;
            }
            if (String.IsNullOrEmpty(textBoxMail.Text))
            {
                MessageBox.Show("Insira o seu email");
                return false; 
            }
            return true;
        }
        #endregion
        #region Tratamento das chaves para string
        //Função para transformar as listas numa string
        /* 
           * Não recebe nada
           * Devolve uma string com a chave N,N,N,N,N;E,E
         */
        private string EuroToString()
        {
            var send = String.Join(",", Numbers.ConvertAll<string>(delegate (int i) { return i.ToString(); }).ToArray());
            send += ";";
            send += String.Join(",", Stars.ConvertAll<string>(delegate (int i) { return i.ToString(); }).ToArray());
            return send;
        }
        #endregion
        #region Clique "Criar Aposta"
        //Função dos clique da uma nova aposta
        /* 
          * Recebe parâmetros o botao clicado
          * Não retorna nada
        */
        private async void btAddAposta_Click(object sender, EventArgs e)
        {
            #region Chaves
            ClearLists();
            if(!IsKey(gbKey.Controls.OfType<TextBox>())) return;
            SortLists();
            var send = EuroToString();
            #endregion
            #region Dados
            if (!IsDataValid()) return;
            #endregion
            #region Mandar para o servidor
            var user = new User { Id = 1, Name = textBoxNome.Text, Email = textBoxMail.Text};
            var key = new Key { Key_ = send };
            var bet = new Bet { UserId = user, Key = key };
            var reply = await Program.client.CreateBetAsync(bet);
            #endregion
            #region Tratamento da resposta
            MessageBox.Show(reply.Message);
            #endregion
        }
        #endregion
        #region Clique "Carregar Apostas"
        //Função dos clique de carregar as apostas do utilizador
        /* 
          * Recebe parâmetros o botao clicado
          * Não retorna nada
        */
        private async void btLoadApostas_Click(object sender, EventArgs e)
        {
            //Limpar a lista cada vez que fazemos um novo pedido
            listBoxApostas.Items.Clear();
            #region Dados
            if (!IsDataValid()) return;
            #endregion
            #region Mandar para o servidor
            var user = new User { Id = 1, Name = textBoxNome.Text, Email = textBoxMail.Text };
            var reply = await Program.client.ConsultBetAsync(user);
            #endregion
            #region Tratamento da resposta
            foreach (var empl in reply.Key)
            {
                listBoxApostas.Items.Add(empl.Key_);
            }
            #endregion
        }
        #endregion
        #region Clique "Sair"
        // Função para retornar à janela de escolher role e ip;
        private void BtSair_Click(object sender, EventArgs e) => Program.mainView.OpenChildForm(new ChooseRole());
        #endregion
    }
}
