﻿
namespace EuroClient___Forms.Views
{
    partial class Admin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.BT_2 = new System.Windows.Forms.Button();
            this.btUsersKey = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.Utilizador = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Chave = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Data = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btSair = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // BT_2
            // 
            this.BT_2.Font = new System.Drawing.Font("Segoe UI", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.BT_2.ForeColor = System.Drawing.Color.Black;
            this.BT_2.Location = new System.Drawing.Point(178, 311);
            this.BT_2.Name = "BT_2";
            this.BT_2.Size = new System.Drawing.Size(376, 95);
            this.BT_2.TabIndex = 3;
            this.BT_2.Text = "Arquivar Apostas";
            this.BT_2.UseVisualStyleBackColor = true;
            this.BT_2.Click += new System.EventHandler(this.BT_2_ClickAsync);
            // 
            // btUsersKey
            // 
            this.btUsersKey.Font = new System.Drawing.Font("Segoe UI", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btUsersKey.ForeColor = System.Drawing.Color.Black;
            this.btUsersKey.Location = new System.Drawing.Point(178, 110);
            this.btUsersKey.Name = "btUsersKey";
            this.btUsersKey.Size = new System.Drawing.Size(376, 95);
            this.btUsersKey.TabIndex = 4;
            this.btUsersKey.Text = "Mostrar apostas";
            this.btUsersKey.UseVisualStyleBackColor = true;
            this.btUsersKey.Click += new System.EventHandler(this.btUsersKey_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Utilizador,
            this.Chave,
            this.Data});
            this.dataGridView1.Location = new System.Drawing.Point(585, 110);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersWidth = 51;
            this.dataGridView1.RowTemplate.Height = 25;
            this.dataGridView1.Size = new System.Drawing.Size(429, 296);
            this.dataGridView1.TabIndex = 5;
            // 
            // Utilizador
            // 
            this.Utilizador.HeaderText = "Utilizador";
            this.Utilizador.MinimumWidth = 6;
            this.Utilizador.Name = "Utilizador";
            this.Utilizador.Width = 125;
            // 
            // Chave
            // 
            this.Chave.HeaderText = "Chave";
            this.Chave.MinimumWidth = 6;
            this.Chave.Name = "Chave";
            this.Chave.Width = 125;
            // 
            // Data
            // 
            this.Data.HeaderText = "Data";
            this.Data.MinimumWidth = 6;
            this.Data.Name = "Data";
            this.Data.Width = 125;
            // 
            // btSair
            // 
            this.btSair.Font = new System.Drawing.Font("Ubuntu", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btSair.ForeColor = System.Drawing.Color.Black;
            this.btSair.Location = new System.Drawing.Point(29, 476);
            this.btSair.Name = "btSair";
            this.btSair.Size = new System.Drawing.Size(157, 47);
            this.btSair.TabIndex = 16;
            this.btSair.Text = "Sair";
            this.btSair.UseVisualStyleBackColor = true;
            this.btSair.Click += new System.EventHandler(this.BtSair_Click);
            // 
            // Admin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1120, 540);
            this.Controls.Add(this.btSair);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.btUsersKey);
            this.Controls.Add(this.BT_2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Admin";
            this.Text = "Admin";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button BT_2;
        private System.Windows.Forms.Button btUsersKey;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Utilizador;
        private System.Windows.Forms.DataGridViewTextBoxColumn Chave;
        private System.Windows.Forms.DataGridViewTextBoxColumn Data;
        private System.Windows.Forms.Button btSair;
    }
}