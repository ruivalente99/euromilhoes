﻿
namespace EuroClient___Forms.Views
{
    partial class ChooseRole
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbIP = new System.Windows.Forms.TextBox();
            this.gbBts = new System.Windows.Forms.GroupBox();
            this.rbtAdmin = new System.Windows.Forms.RadioButton();
            this.rbtGestor = new System.Windows.Forms.RadioButton();
            this.rbtCliente = new System.Windows.Forms.RadioButton();
            this.btGo = new System.Windows.Forms.Button();
            this.gbBts.SuspendLayout();
            this.SuspendLayout();
            // 
            // tbIP
            // 
            this.tbIP.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbIP.Font = new System.Drawing.Font("Ubuntu", 28.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.tbIP.Location = new System.Drawing.Point(274, 176);
            this.tbIP.Name = "tbIP";
            this.tbIP.PlaceholderText = "IP";
            this.tbIP.Size = new System.Drawing.Size(740, 62);
            this.tbIP.TabIndex = 0;
            // 
            // gbBts
            // 
            this.gbBts.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbBts.BackColor = System.Drawing.Color.Transparent;
            this.gbBts.Controls.Add(this.rbtAdmin);
            this.gbBts.Controls.Add(this.rbtGestor);
            this.gbBts.Controls.Add(this.rbtCliente);
            this.gbBts.Location = new System.Drawing.Point(274, 267);
            this.gbBts.Name = "gbBts";
            this.gbBts.Size = new System.Drawing.Size(513, 91);
            this.gbBts.TabIndex = 1;
            this.gbBts.TabStop = false;
            // 
            // rbtAdmin
            // 
            this.rbtAdmin.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rbtAdmin.AutoSize = true;
            this.rbtAdmin.Font = new System.Drawing.Font("Ubuntu", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.rbtAdmin.ForeColor = System.Drawing.Color.Black;
            this.rbtAdmin.Location = new System.Drawing.Point(344, 27);
            this.rbtAdmin.Name = "rbtAdmin";
            this.rbtAdmin.Size = new System.Drawing.Size(123, 39);
            this.rbtAdmin.TabIndex = 2;
            this.rbtAdmin.TabStop = true;
            this.rbtAdmin.Text = "Admin";
            this.rbtAdmin.UseVisualStyleBackColor = true;
            // 
            // rbtGestor
            // 
            this.rbtGestor.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rbtGestor.AutoSize = true;
            this.rbtGestor.Font = new System.Drawing.Font("Ubuntu", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.rbtGestor.ForeColor = System.Drawing.Color.Black;
            this.rbtGestor.Location = new System.Drawing.Point(185, 26);
            this.rbtGestor.Name = "rbtGestor";
            this.rbtGestor.Size = new System.Drawing.Size(128, 39);
            this.rbtGestor.TabIndex = 1;
            this.rbtGestor.TabStop = true;
            this.rbtGestor.Text = "Gestor";
            this.rbtGestor.UseVisualStyleBackColor = true;
            // 
            // rbtCliente
            // 
            this.rbtCliente.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rbtCliente.AutoSize = true;
            this.rbtCliente.Font = new System.Drawing.Font("Ubuntu", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.rbtCliente.ForeColor = System.Drawing.Color.Black;
            this.rbtCliente.Location = new System.Drawing.Point(7, 27);
            this.rbtCliente.Name = "rbtCliente";
            this.rbtCliente.Size = new System.Drawing.Size(133, 39);
            this.rbtCliente.TabIndex = 0;
            this.rbtCliente.TabStop = true;
            this.rbtCliente.Text = "Cliente";
            this.rbtCliente.UseVisualStyleBackColor = true;
            // 
            // btGo
            // 
            this.btGo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btGo.Font = new System.Drawing.Font("Ubuntu", 22.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btGo.ForeColor = System.Drawing.Color.Black;
            this.btGo.Location = new System.Drawing.Point(806, 276);
            this.btGo.Name = "btGo";
            this.btGo.Size = new System.Drawing.Size(210, 80);
            this.btGo.TabIndex = 2;
            this.btGo.Text = "IR";
            this.btGo.UseVisualStyleBackColor = true;
            this.btGo.Click += new System.EventHandler(this.btGo_Click);
            // 
            // ChooseRole
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(1280, 720);
            this.Controls.Add(this.gbBts);
            this.Controls.Add(this.btGo);
            this.Controls.Add(this.tbIP);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "ChooseRole";
            this.Text = "ChooseRole";
            this.gbBts.ResumeLayout(false);
            this.gbBts.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbIP;
        private System.Windows.Forms.GroupBox gbBts;
        private System.Windows.Forms.RadioButton rbtAdmin;
        private System.Windows.Forms.RadioButton rbtGestor;
        private System.Windows.Forms.RadioButton rbtCliente;
        private System.Windows.Forms.Button btGo;
    }
}