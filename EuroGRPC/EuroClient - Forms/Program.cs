using EuroClient___Forms.Proto;
using EuroClient___Forms.Views;
using Grpc.Net.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EuroClient___Forms
{
    static class Program
    {
        public static Main mainView = new Main();
        #region Conex�o com o servidor
            public static string ip;
            public static GrpcChannel channel;
            public static EuroBet.EuroBetClient client;
        #endregion
        /// <summary>
        ///  The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.SetHighDpiMode(HighDpiMode.SystemAware);
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(mainView);
        }
    }
}
