﻿using EuroGRPC.Protos;
using Grpc.Net.Client;
using System;
using System.Collections.ObjectModel;

namespace EuroClient
{
    class Program
    {
        static async System.Threading.Tasks.Task Main(string[] args)
        {
            var user = new User { Id = 1, Name = "Valente", Email = "al68636@utad.eu " };
            var key = new Key { Key_ = "1 2 3 4 5 1 2" };
            var bet = new Bet { UserId = user, Key = key };
            var channel = GrpcChannel.ForAddress("https://localhost:5001");
            var client = new EuroBet.EuroBetClient(channel);
            var reply = await client.CreateBetAsync(bet);
            Console.WriteLine(reply);
            Console.ReadLine();
        }
    }
}
