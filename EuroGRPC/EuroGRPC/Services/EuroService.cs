﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using EuroGRPC.Protos;
using Google.Protobuf.WellKnownTypes;
using Grpc.Core;
using Microsoft.Extensions.Logging;
using System.Data;
using System.Data.SqlClient;
namespace EuroGRPC.Services
{
    public class EuroService : EuroBet.EuroBetBase
    {
        //Connection String com a base de daos
        public string con = "Server=(localdb)\\MSSQLLocalDB;Database=EuroMilhoes;Trusted_Connection=True;";
        #region Logger
        private readonly ILogger<EuroService> _logger;
        public EuroService(ILogger<EuroService> logger)
        {
            _logger = logger;
        }
        #endregion
        #region Serviços
        //Função para criar uma aposta
        /* 
          * Recebendo uma aposta (Bet), constituida por uma chave(Key) e dados do utilizador(User) definidos pelo nome e o email, 
          * de seguida é verificada a exitencia o utilizador e caso não exista, o mesmo é criado.
          * Após tal, é inserido na base de dados a nova aposta, na tabela Ative_Key
          * Retorna ao Client a confirmação da aposta(Bet confirmation)
          * 
        */
        public override Task<BetConfirmation> CreateBet(Bet request, ServerCallContext context)
        {
            BetConfirmation output = new BetConfirmation();

            string sql = "insert into EuroUser (name, email) values(@first,@last)";
            using (SqlConnection cnn = new SqlConnection(con))
            {
                try
                {
                   
                    cnn.Open();
                    int UserId = 0;
                    SqlCommand command = new SqlCommand("Select id from [EuroUser] where email=@mail", cnn);
                    command.Parameters.AddWithValue("@mail", request.UserId.Email);
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            UserId = (int)reader["id"];
                        }
                    }
                    if (UserId == 0)
                    {
                        using (SqlCommand cmd = new SqlCommand(sql, cnn))
                        {

                            cmd.Parameters.Add("@first", SqlDbType.NVarChar).Value = request.UserId.Name;
                            cmd.Parameters.Add("@last", SqlDbType.NVarChar).Value = request.UserId.Email;


                            int rowsAdded = cmd.ExecuteNonQuery();


                        }
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            if (reader.Read())
                            {
                                UserId = (int)reader["id"];
                            }
                        }
                    }
                    sql = "insert into Ative_Key(eurokey,data,EuroUserId) values(@first,@second,@third) ";
                    using (SqlCommand cmd = new SqlCommand(sql, cnn))
                    {
                        // Create and set the parameters values 
                        cmd.Parameters.Add("@first", SqlDbType.NVarChar).Value = request.Key.Key_;
                        cmd.Parameters.Add("@second", SqlDbType.NVarChar).Value = DateTime.UtcNow;
                        cmd.Parameters.Add("@third", SqlDbType.NVarChar).Value = UserId;
                        int rowsAdded = cmd.ExecuteNonQuery();
                    }
                }
                catch (Exception ex)
                {
                    output.Message = "ERROR:" + ex.Message;
                    return Task.FromResult(output);
                }
            }
            output.Message = "Aposta efetuada: " + request.Key + ", feita por " + request.UserId.Name;
            output.Date = Timestamp.FromDateTime(DateTime.UtcNow);
            return Task.FromResult(output);
        }

        //Função para consultar aposta
        /*
           * Recebendo dados do utilizador(User), pesquisamos a exitencia do mesmo, 
           * Retorna ao Client todas as apostas que o mesmo fez (UserBet)
         */
        public override Task<UserBet> ConsultBet(User request, ServerCallContext context)
        {
            UserBet output = new UserBet();
            using (SqlConnection cnn = new SqlConnection(con))
            {
                try
                {

                    cnn.Open();
                    int UserId = 0;
                    SqlCommand command = new SqlCommand("Select id from [EuroUser] where email=@mail", cnn);
                    command.Parameters.AddWithValue("@mail", request.Email);
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            UserId = (int)reader["id"];
                        }
                    }
                    if (UserId == 0)
                    {

                    }
                    SqlCommand command2 = new SqlCommand("Select * from [Ative_Key] where EuroUserId=@id", cnn);
                    command2.Parameters.AddWithValue("@id", UserId);
                    List<string> chaves = new List<string>();
                    using (SqlDataReader reader = command2.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            output.Key.Add(new Key { Key_ = reader["eurokey"].ToString() });

                        }
                    }

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    //output.Message = "ERROR:" + ex.Message;
                    return Task.FromResult(output);
                }
            }
            return Task.FromResult(output);
        }

        //Função para consultar os vencedores
        /*
           * Recebendo uma chave(Key) 
           * é efetuada uma pesquisa na base de dados pelos utilizadores que realizaram a aposta com aquela chave 
           * Retorna a lista dos mesmos para o Client (Winners)
         */
        public override Task<Winners> ConsultWinners(Key request, ServerCallContext context)
        {
            List<int> winner = new List<int>();
            Winners output = new Winners();
            using (SqlConnection cnn = new SqlConnection(con))
            {
                try
                {

                    cnn.Open();

                    SqlCommand command = new SqlCommand("Select * from [Ative_Key] where eurokey='" + request.Key_.ToString() + "'", cnn);
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            winner.Add(Convert.ToInt32(reader["EuroUserId"]));

                        }
                    }
                    foreach (int id in winner)
                    {
                        command = new SqlCommand("Select * from [EuroUser] where id=" + id.ToString(), cnn);
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                output.UserId.Add(new User { Name = reader["name"].ToString(), Email = reader["email"].ToString() });

                            }
                        }
                    }

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    //output.Message = "ERROR:" + ex.Message;
                    return Task.FromResult(output);
                }
            }
            return Task.FromResult(output);
        }

        //Função para consultar TODAS as apostas
        /*
           * Efetuada uma pesquisa na base de dados por todas as apostas efetuadas e ativas
           * Retorna a lista das mesmas e dos utilizadores à qual referem essas apostas (AllBets)
         */
        public override Task<AllBets> ConsultAll(All request, ServerCallContext context)
        {
            AllBets output = new AllBets();
            using (SqlConnection cnn = new SqlConnection(con))
            {
                try
                {
                    cnn.Open();
                    SqlCommand command = new SqlCommand("Select * from [Ative_Key]", cnn);
                    List<string> chaves = new List<string>();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            output.Key.Add(new Key { Key_ = reader["eurokey"].ToString() });
                            output.UserId.Add(new User { Id = Convert.ToInt32(reader["EuroUserId"]) });
                        }
                    }
                    foreach (var users in output.UserId)
                    {
                        command = new SqlCommand("Select * from [EuroUser] where id = " + users.Id, cnn);
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                users.Email = reader["email"].ToString();
                                users.Name = reader["name"].ToString();
                            }
                        }
                    }

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    //return Task.FromResult(output);
                }
            }
            return Task.FromResult(output);
        }
        //Função para arquivar as apostas
        /*
           * Todas as apostas na tabela Ative_Key são copiadas para a tabela Arqui_Key, 
           * de seguida os dados da tabela Ative_Key são eliminados
           * Retorna uma mensagem de confirmação
         */
        public override Task<All> Arquive(All request, ServerCallContext context)
        {
            All output = new All();
            string sql = "INSERT INTO [Arqui_Key] SELECT* FROM[Ative_Key] delete from Ative_Key";
            using (SqlConnection cnn = new SqlConnection(con))
            {
                cnn.Open();
                try
                {
                    using (SqlCommand cmd = new SqlCommand(sql, cnn))
                    {
                        cmd.ExecuteNonQuery();
                        output.Msg = "Arquivado";
                    }
                }
                catch (Exception ex)
                {
                    output.Msg = "ERROR:" + ex.Message;
                    return Task.FromResult(output);
                }
                return Task.FromResult(output);
            }
        }
        #endregion
    }
}
